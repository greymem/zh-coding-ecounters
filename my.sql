
CREATE TABLE `villages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  UNIQUE KEY `villages_id_IDX` (`id`) USING BTREE,
  UNIQUE KEY `villages_name_IDX` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `counters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `village_id` int(11) NOT NULL,
  UNIQUE KEY `counters_id_IDX` (`id`) USING BTREE,
  KEY `counters_FK` (`village_id`),
  CONSTRAINT `counters_FK` FOREIGN KEY (`village_id`) REFERENCES `villages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `metrics` (
  `counter_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `moment` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `metrics_FK` (`counter_id`),
  CONSTRAINT `metrics_FK` FOREIGN KEY (`counter_id`) REFERENCES `counters` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci


----- DATA -----
INSERT INTO `zh-coding-ecounters`.villages (name) VALUES 
('Villabajo')
,('Villarriba');

INSERT INTO `zh-coding-ecounters`.counters (village_id) VALUES 
(1)
,(2);