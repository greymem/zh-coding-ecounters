package de.zenhomes.ecounters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@EnableAutoConfiguration
public class ECApplication {
    public static void main(String[] args) {
        SpringApplication.run(ECApplication.class, args);
    }
}