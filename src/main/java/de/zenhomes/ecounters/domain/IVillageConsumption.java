package de.zenhomes.ecounters.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface IVillageConsumption extends Serializable {
	
	@JsonProperty("village_name")
	String getVillageName();
	
	void setVillageName(String villageName);
	double getConsumption();
	void setConsumption(double consumption);
}