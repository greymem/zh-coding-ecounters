package de.zenhomes.ecounters.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MetricDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonIgnore
	private int counterId;
	private double amount;
	
	@JsonProperty("counter_id")
	public void setCounterId(String counterIdStr) {
		this.counterId = Integer.parseInt(counterIdStr);
	}
}