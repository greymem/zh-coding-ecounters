package de.zenhomes.ecounters.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class VillageCounterDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	@JsonProperty("village_name")
	private String villageName;
	
	public VillageCounterDTO() {
		//
	}
	
	public VillageCounterDTO(String counterId, String villageName) {
		this.id=counterId;
		this.villageName=villageName;
	}
}