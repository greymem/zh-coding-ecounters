package de.zenhomes.ecounters.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Villages implements Serializable {
	private static final long serialVersionUID = 1L;
	private Collection<IVillageConsumption> villages;
	
	public Villages(Collection<IVillageConsumption> villages) {
		this.villages=villages;
	}
}