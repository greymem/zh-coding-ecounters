package de.zenhomes.ecounters.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import de.zenhomes.ecounters.entity.Village;

@Repository
public interface VillageRepository extends JpaRepository<Village, Long> {
	@Query("select c from Village c order by c.id")
	Collection<Village> getAllVillages();
	
	@Query("select c from Village c where c.id=:id")
	Village getVillage(@Param("id") int id);
}

