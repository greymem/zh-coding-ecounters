package de.zenhomes.ecounters.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import de.zenhomes.ecounters.domain.IVillageConsumption;
import de.zenhomes.ecounters.entity.Metric;

@Repository
public interface MetricRepository extends JpaRepository<Metric, Long> {
	@Query("select c from Metric c order by c.id")
	List<Metric> getAllMetrics();
	
	@Query("select c from Metric c where c.id=:id")
	Metric getMetric(@Param("id") int id);
	
	@Query("select v.name as villageName, sum(m.amount) as consumption from Village v "
			+ " left join Counter c on v.id = c.village.id"
			+ " left join Metric m on c.id=m.counterId"
			+ " where m.moment>=:moment"
			+ " group by v.name")
	Collection<IVillageConsumption> getVillageConsumption(@Param("moment") Date moment);
}

