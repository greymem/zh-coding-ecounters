package de.zenhomes.ecounters.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import de.zenhomes.ecounters.entity.Counter;

@Repository
public interface CounterRepository extends JpaRepository<Counter, Long> {
	@Query("select c from Counter c order by c.id")
	List<Counter> getAllCounters();
	
	@Query("select c from Counter c where c.id=:id")
	Counter getCounter(@Param("id") int id);
}

