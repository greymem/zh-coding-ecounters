package de.zenhomes.ecounters.service;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.zenhomes.ecounters.domain.Villages;
import de.zenhomes.ecounters.repository.MetricRepository;

@Service
public class ConsumptionService {

	@Autowired
	MetricRepository metricRepository;
	
	private static final Pattern DURATION_PATTERN = Pattern.compile("([0-9]+)([h|m|s])", Pattern.MULTILINE);

	public Villages getVillagesConsumption(String durationStr) {
		final Matcher matcher = DURATION_PATTERN.matcher(durationStr);
		if (matcher.matches()) {
			long durationSeconds = getDurationInSeconds(Integer.parseInt(matcher.group(1)), matcher.group(2).charAt(0));
			
			Date dateFrom = new Date(System.currentTimeMillis()-(durationSeconds*1000));
			return new Villages(metricRepository.getVillageConsumption(dateFrom));
		} else {
			throw new IllegalArgumentException(
					StringUtils.join("Wrong duration syntax. Correct:", DURATION_PATTERN.pattern()));
		}
	}

	public long getDurationInSeconds(long duraton, char durationPeriod) {
		switch (durationPeriod) {
		case 'h':
			duraton *= 60 * 60;
			break;
		case 'm':
			duraton *= 60;
			break;
		default:
			break;
		}
		return duraton;
	}
}
