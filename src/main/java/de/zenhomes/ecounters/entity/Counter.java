package de.zenhomes.ecounters.entity;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * The persistent class for the counters database table.
 * 
 */
@Entity
@Getter @Setter
@Table(name="counters")
@NamedQuery(name="Counter.findAll", query="SELECT c FROM Counter c")
public class Counter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	private java.lang.Integer id;
	
	//bi-directional many-to-one association to Village
	@ManyToOne
	@JoinColumn(name="village_id", referencedColumnName="id")
	private Village village;

	public Counter() {
		//
	}
	
	public Counter(Village village) {
		this.setVillage(village);
	}
}