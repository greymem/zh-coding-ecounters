package de.zenhomes.ecounters.entity;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import java.util.List;

/**
 * The persistent class for the villages database table.
 * 
 */
@Entity
@Table(name = "villages")
@NamedQuery(name = "Village.findAll", query = "SELECT v FROM Village v")
public class Village implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonProperty(access = Access.READ_ONLY)
	@Column(name = "id", insertable = false, updatable = false)
	private java.lang.Integer id;

	@Column(name = "name")
	private String name;

	// bi-directional many-to-one association to Counter
	@JsonIgnore
	@OneToMany(mappedBy = "village")
	private List<Counter> counters;

	public Village() {
		//
	}
	
	public Village(String name) {
		this.name=name;
	}

	public java.lang.Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}