package de.zenhomes.ecounters.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;


/**
 * The persistent class for the metrics database table.
 * 
 */
@Entity
@Table(name="metrics")
@NamedQuery(name="Metric.findAll", query="SELECT m FROM Metric m")
public class Metric implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name = "id", insertable = false, updatable = false)
	private int id;
	
	@JsonIgnore
	@Column(name="counter_id")
	private int counterId;
	
	@JsonIgnore
	@Column(name="moment", insertable = false, updatable = false)
	private Timestamp moment;
	
	@Column(name="amount")
	private double amount;

	public Metric() {
		//
	}
	
	public Metric(int counterId, double amount) {
		this.counterId=counterId;
		this.amount=amount;
	}
	
	public int getCounterId() {
		return counterId;
	}
	
	@JsonIgnore
	public void setCounterId(int counterId) {
		this.counterId = counterId;
	}
	
	@JsonProperty("counter_id")
	public void setCounterId(String counterIdStr) {
		this.counterId = Integer.parseInt(counterIdStr);
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}