package de.zenhomes.ecounters.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.zenhomes.ecounters.entity.Village;
import de.zenhomes.ecounters.repository.VillageRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.bytebuddy.build.Plugin.Engine.Source.Empty;

@Api(value = "Villages")
@RestController
@RequestMapping("/villages")
public class VillageController {

	@Autowired
	VillageRepository villageRepository;

	@ApiOperation(value = "All villages", notes = "Get all villages")
	@GetMapping(value = "/")
	public ResponseEntity<Collection<Village>> getAllVillages() {
		return ResponseEntity.ok().body(villageRepository.getAllVillages());
	}
	
	@ApiOperation(value = "Add village", notes = "Add new village")
	@PostMapping(value = "/")
	public ResponseEntity<Village> addVillage(String villageName) {
		return ResponseEntity.ok().body(villageRepository.save(new Village(villageName)));
	}
	
	@ApiOperation(value = "Get village", notes = "Get village by id")
	@GetMapping(value = "/{villageId}")
	public ResponseEntity<Village> getVillage(@PathVariable int villageId) {
		return ResponseEntity.ok().body(villageRepository.getVillage(villageId));
	}

	@ApiOperation(value = "Delete village", notes = "Delete village by id")
	@DeleteMapping(value = "/{villageId}")
	public ResponseEntity<Empty> deleteVillage(@PathVariable int villageId) {
		villageRepository.delete(villageRepository.getVillage(villageId));
		return ResponseEntity.ok().build();
	}
}
