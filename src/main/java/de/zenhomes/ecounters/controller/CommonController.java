package de.zenhomes.ecounters.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.zenhomes.ecounters.domain.MetricDTO;
import de.zenhomes.ecounters.domain.Villages;
import de.zenhomes.ecounters.domain.VillageCounterDTO;
import de.zenhomes.ecounters.entity.Counter;
import de.zenhomes.ecounters.entity.Metric;
import de.zenhomes.ecounters.repository.CounterRepository;
import de.zenhomes.ecounters.repository.MetricRepository;
import de.zenhomes.ecounters.service.ConsumptionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "Misc operations")
@RestController
@RequestMapping("/")
public class CommonController {

	@Autowired
	MetricRepository metricRepository;
	
	@Autowired
	CounterRepository counterRepository;
	
	@Autowired
	ConsumptionService consumptionService;
	
	@ApiOperation(value = "Counter callback", notes = "Post counter metrics")
	@PostMapping(value = "/counter_callback")
	public ResponseEntity<Metric> counterCallback(@RequestBody MetricDTO counterMetric) {
		return ResponseEntity.ok()
				.body(metricRepository.save(new Metric(counterMetric.getCounterId(), counterMetric.getAmount())));
	}

	@ApiOperation(value = "Counter info", notes = "Get counter info by counter id")
	@GetMapping(value = "/counter")
	public ResponseEntity<VillageCounterDTO> getCounterInfo(@RequestParam(required = true) int id) {
		Counter counter = counterRepository.getCounter(id);
		return ResponseEntity.ok()
				.body(new VillageCounterDTO(counter.getId().toString(), counter.getVillage().getName()));
	}

	@ApiOperation(value = "Consumption report", notes = "Get consumption report")
	@GetMapping(value = "/consumption_report")
	public ResponseEntity<Villages> getConsumptionReport(
			@RequestParam(required = true) @ApiParam("Example: '1h' / '60m' / '360s'") String duration) {
		return ResponseEntity.ok().body(consumptionService.getVillagesConsumption(duration));
	}
}
