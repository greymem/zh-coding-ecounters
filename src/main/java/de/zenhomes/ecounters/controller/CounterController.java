package de.zenhomes.ecounters.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.zenhomes.ecounters.domain.VillageCounterDTO;

import de.zenhomes.ecounters.entity.Counter;
import de.zenhomes.ecounters.entity.Village;
import de.zenhomes.ecounters.repository.CounterRepository;
import de.zenhomes.ecounters.repository.VillageRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.bytebuddy.build.Plugin.Engine.Source.Empty;

@Api(value = "Counters")
@RestController
@RequestMapping("/counters")
public class CounterController {

	@Autowired
	CounterRepository counterRepository;
	
	@Autowired
	VillageRepository villageRepository;
	
	@ApiOperation(value = "Add counter", notes = "Add new counter to village. By village id")
	@PostMapping(value = "/")
	public ResponseEntity<VillageCounterDTO> addCounter(int villageId) {
		Village village = villageRepository.getVillage(villageId);
		Counter newCounter = counterRepository.save(new Counter(village));
		return ResponseEntity.ok().body(new VillageCounterDTO(newCounter.getId().toString(), village.getName()));
	}
	
	@ApiOperation(value = "Get counter", notes = "Get counter by id")
	@GetMapping(value = "/{counterId}")
	public ResponseEntity<Counter> getCounter(@PathVariable int counterId) {
		return ResponseEntity.ok().body(counterRepository.getCounter(counterId));
	}
	
	@ApiOperation(value = "Delete counter", notes = "Delete counter by id")
	@DeleteMapping(value = "/{counterId}")
	public ResponseEntity<Empty> deleteCounter(@PathVariable int counterId) {
		counterRepository.delete(counterRepository.getCounter(counterId));
		return ResponseEntity.ok().build();
	}
}
