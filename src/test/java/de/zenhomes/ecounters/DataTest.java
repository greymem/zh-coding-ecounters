package de.zenhomes.ecounters;

import java.util.Collection;
import java.util.Date;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import de.zenhomes.ecounters.domain.IVillageConsumption;
import de.zenhomes.ecounters.entity.Counter;
import de.zenhomes.ecounters.entity.Metric;
import de.zenhomes.ecounters.entity.Village;
import de.zenhomes.ecounters.repository.CounterRepository;
import de.zenhomes.ecounters.repository.MetricRepository;
import de.zenhomes.ecounters.repository.VillageRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class DataTest extends Assert {

    @Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private VillageRepository villageRepository;
    
    @Autowired
    private CounterRepository counterRepository;
    
    @Autowired
    private MetricRepository metricRepository;
    
	@Test
	public void canCreateVillage() {
		Village fairy = createAndSaveVillage("Fairy");
	    Village found = villageRepository.getVillage(fairy.getId());
	    
	    // then
	    assertTrue(found.getName().equals(fairy.getName()));
	}
	
	@Test
	public void canCreateCounter() {
		Village tail = createAndSaveVillage("Tail");
		Counter counter = createAndSaveCounter(tail);
		Counter found = counterRepository.getCounter(counter.getId());

		// then
		assertTrue(found.getId().equals(counter.getId()) && found.getVillage().getName().equals(tail.getName()));
	}
	
	@Test
	public void canCreateMetricAndGetVillageConsumption() {
		long millis = System.currentTimeMillis();
		
		Village fairytail = createAndSaveVillage("Fairytail");
		Counter counter1 = createAndSaveCounter(fairytail);
		Counter counter2 = createAndSaveCounter(fairytail);
		
		double amount1 = Math.random()+(Math.random()*10);
		double amount2 = Math.random()+(Math.random()*10);
		
		saveMetric(new Metric(counter1.getId(), amount1));
		saveMetric(new Metric(counter2.getId(), amount2));
		
		Collection<IVillageConsumption> found = metricRepository.getVillageConsumption(new Date(millis-1));
		for (IVillageConsumption villageConsumption : found) {
			
			if (villageConsumption.getVillageName().equals(fairytail.getName())) {
				assertTrue(villageConsumption.getConsumption()==amount1+amount2);
			}
		}
	}
	
	private Village createAndSaveVillage(String villageName) {
		Village village = new Village("Fairy");
		village = entityManager.persist(village);
	    entityManager.flush();
	    return village;
	}
	
	private Counter createAndSaveCounter(Village village) {
		Counter counter = new Counter(village);
		counter = entityManager.persist(counter);
	    entityManager.flush();
	    return counter;
	}
	
	private Metric saveMetric(Metric metric) {
		metric = entityManager.persist(metric);
	    entityManager.flush();
	    entityManager.clear();
	    return metric;
	}
}
