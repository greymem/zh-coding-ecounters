# zh-coding-ecounters

**Installation**

* Apply 'my.sql' script in your MySQL DB

* Specify MySQL connection url *(jdbc:mysql://<host>:<port>/<db_name>)*, username and password in /src/main/resources/application.yml *(spring.datasource.)*